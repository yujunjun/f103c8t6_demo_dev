#ifndef _BSP_BUZZER_H
#define _BSP_BUZZER_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f1xx_hal.h"  
#include "main.h"

#define BUZZER_ON() HAL_GPIO_WritePin(Buzzer_GPIO_Port, Buzzer_Pin,GPIO_PIN_SET)
#define BUZZER_OFF() HAL_GPIO_WritePin(Buzzer_GPIO_Port, Buzzer_Pin,GPIO_PIN_RESET)

#ifdef __cplusplus
}
#endif

#endif /* _BSP_BUZZER_H */



