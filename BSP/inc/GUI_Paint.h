#ifndef __GUI_PAINT_H
#define __GUI_PAINT_H

#include "DEV_Config.h"
/**
 * Image attributes
**/
typedef struct {
    UBYTE *Image;
    UWORD Width;
    UWORD Height;
    UWORD WidthMemory;
    UWORD HeightMemory;
    UWORD Color;
    UWORD Rotate;
    UWORD Mirror;
    UWORD WidthByte;
    UWORD HeightByte;
    UWORD Scale;
} PAINT;
extern PAINT Paint;

#define WHITE          0xFF
#define BLACK          0x00

#define  GRAY1 0x03 //Blackest
#define  GRAY2 0x02
#define  GRAY3 0x01 //gray
#define  GRAY4 0x00 //white

void Paint_NewImage(UBYTE *image, UWORD Width, UWORD Height, UWORD Rotate, UWORD Color);
void Paint_Clear(UWORD Color);
void Paint_SelectImage(UBYTE *image);
void Paint_DrawBitMap(const unsigned char* image_buffer);
#endif
