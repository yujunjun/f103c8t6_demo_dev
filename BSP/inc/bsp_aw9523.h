#ifndef _BSP_AW9523_H
#define _BSP_AW9523_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"

/*
LED_00_ON | LED_01_ON | LED_02_ON | LED_03_ON |
LED_10_ON | LED_11_ON | LED_12_ON | LED_13_ON |
LED_20_ON | LED_21_ON | LED_22_ON | LED_23_ON |
LED_30_ON | LED_31_ON | LED_32_ON | LED_33_ON |
*/

#define AW9523_RST_GPIO_Port GPIOB
#define AW9523_RST_Pin       GPIO_PIN_5
#define AW9523_WADDR         0xb6
#define AW9523_RADDR         0xb7

#define AW9523_ID            0x10
#define AW9523_CTRL          0x11
#define AW9523_LED_MODE0     0x12
#define AW9523_LED_MODE1     0x13

#define AW9523_LED0          0x20

#define LED_00_ON     (0x01)
#define LED_10_ON     (0x01 << 1)
#define LED_20_ON     (0x01 << 2)
#define LED_30_ON     (0x01 << 3)
#define LED_01_ON     (0x01 << 4)
#define LED_11_ON     (0x01 << 5)
#define LED_21_ON     (0x01 << 6)
#define LED_31_ON     (0x01 << 7)
#define LED_02_ON     (0x01 << 8)
#define LED_12_ON     (0x01 << 9)
#define LED_22_ON     (0x01 << 10)
#define LED_32_ON     (0x01 << 11)
#define LED_03_ON     (0x01 << 12)
#define LED_13_ON     (0x01 << 13)
#define LED_23_ON     (0x01 << 14)
#define LED_33_ON     (0x01 << 15)

#define LED_ALL_ON      0xFFFF
#define LED_ALL_OFF     0x0000

/* menu control */
#define LED_UP_ON       (LED_00_ON | LED_01_ON | LED_02_ON | LED_03_ON)
#define LED_DOWN_ON     (LED_30_ON | LED_31_ON | LED_32_ON | LED_33_ON)
#define LED_LEFT_ON     (LED_00_ON | LED_10_ON | LED_20_ON | LED_30_ON)
#define LED_RIGHT_ON    (LED_03_ON | LED_13_ON | LED_23_ON | LED_33_ON)
#define LED_CENTER_ON   (LED_11_ON | LED_21_ON | LED_12_ON | LED_22_ON)
#define LED_AROUND_ON   (LED_UP_ON | LED_DOWN_ON | LED_LEFT_ON | LED_RIGHT_ON)

/* smart door */
#define OPEN_DOOR       (LED_CENTER_ON)
#define CLOSE_DOOR      (LED_00_ON | LED_03_ON | LED_30_ON | LED_33_ON)

/* face recognition */
#define PEOPLE_FACE     (OPEN_DOOR)

/* finger recognition */
#define FINGER_ONE      (LED_00_ON | LED_01_ON | LED_02_ON | LED_03_ON)
#define FINGER_TWO      (FINGER_ONE | LED_10_ON | LED_11_ON | LED_12_ON | LED_13_ON)
#define FINGER_THREE    (FINGER_TWO | LED_20_ON | LED_21_ON | LED_22_ON | LED_23_ON)
#define FINGER_FOUR     (FINGER_THREE | LED_30_ON | LED_31_ON | LED_32_ON | LED_33_ON)
#define FINGER_FIVE     (LED_CENTER_ON)
#define BACK_GROUND_1   (LED_AROUND_ON)
#define BACK_GROUND_2   (LED_AROUND_ON)
#define BACK_GROUND_3   (LED_AROUND_ON)

/* gesture recognition */
#define GESTURE_UP      (LED_UP_ON)
#define GESTURE_DOWN    (LED_DOWN_ON)
#define GESTURE_LEFT    (LED_LEFT_ON)
#define GESTURE_RIGHT   (LED_RIGHT_ON)
#define GESTURE_AWAKEN  (LED_CENTER_ON)

/* toy control */
#define TOY_UP          (LED_UP_ON)
#define TOY_DOWN        (LED_DOWN_ON)
#define TOY_LEFT        (LED_LEFT_ON)
#define TOY_RIGHT       (LED_RIGHT_ON)
#define TOY_SELECT      (LED_20_ON | LED_31_ON | LED_22_ON | LED_13_ON)
#define TOY_CANCEL      (LED_10_ON | LED_21_ON | LED_32_ON | LED_30_ON | LED_21_ON | LED_12_ON)
#define TOY_PASSING_BY  (LED_11_ON | LED_21_ON | LED_30_ON | LED_32_ON | LED_33_ON)

/* in out door recognition */
#define IN_ON    (LED_11_ON | LED_21_ON | LED_12_ON | LED_22_ON)
#define OUT_ON   (LED_UP_ON | LED_DOWN_ON | LED_LEFT_ON | LED_RIGHT_ON)

#define ONE_ON     (LED_00_ON | LED_01_ON | LED_02_ON | LED_03_ON)
#define TWO_ON     (ONE_ON    | LED_10_ON | LED_11_ON | LED_12_ON | LED_13_ON)
#define THREE_ON   (TWO_ON    | LED_20_ON | LED_21_ON | LED_22_ON | LED_23_ON)
#define FOUR_ON    (THREE_ON  |LED_30_ON | LED_31_ON | LED_32_ON | LED_33_ON)
#define OTHER_ON   (LED_11_ON | LED_21_ON | LED_12_ON | LED_22_ON))
#define ZERO_ON     LED_ALL_OFF

#define AW9523_LED_BRIGHTNESS  0x02 /* 0x00-0xFF*/

void AW9523_Init(void);
void AW9523_LED_CTRL(uint16_t pos);
void AW9523_LED_CTRL_7_11(uint8_t idim);
void AW9523_LED_CTRL_CENTER(uint8_t idim);

#ifdef __cplusplus
}
#endif

#endif /* _BSP_AW9523_H */



