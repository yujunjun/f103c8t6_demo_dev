#ifndef _BSP_READOUT_H
#define _BSP_READOUT_H

#ifdef __cplusplus
 extern "C" {
#endif

/*************************************INCLUDE************************************/
#include "main.h"
/*************************************REGISTER***********************************/
/**************************************MACRO*************************************/

#define SILENCE_TIME                               (900)
#define MAX_WAIT_TIME                              (2000)

typedef enum EVENT{
  NO_EVENT    = 0,
  LEFT_EVENT  = 1,
  RIGHT_EVENT = 2,
  OTHER_EVENT = 3
} Event_t;

typedef enum RESULT{
  NO_RESULT  = 0,
  IN_DOOR  = 1,
  OUT_DOOR = 2
} Result_t;

typedef struct
{
  uint8_t flag;
  Event_t event;
  Result_t result;
  uint16_t inDoorCnt;
  uint16_t outDoorCnt;
  uint32_t waitTime;
  uint8_t refresh;
} Readout_t;

/*************************************FUNCTION***********************************/  

extern Readout_t readout;
Event_t getEvent(void);

#ifdef __cplusplus
}
#endif

#endif /* _BSP_READOUT_H */

/***********************************END OF FILE**********************************/

