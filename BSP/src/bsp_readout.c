#include "bsp_readout.h"

Readout_t readout;

/**
  * @brief  getEvent
  * @param  None
  * @note   
  *         
  * @retval event
  */
Event_t getEvent(void)
{
  Event_t event = NO_EVENT;

  event |= HAL_GPIO_ReadPin(READOUT1_GPIO_Port, READOUT1_Pin) << 1;
  event |= HAL_GPIO_ReadPin(READOUT0_GPIO_Port, READOUT0_Pin);

  return event;
}

/***********************************END OF FILE**********************************/


