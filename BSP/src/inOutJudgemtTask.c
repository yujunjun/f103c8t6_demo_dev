#include "cmsis_os.h"

#include "bsp_readout.h"
#include "bsp_buzzer.h"
#include "bsp_aw9523.h"

void StartInOutJudgmt(void const * argument)
{
  /* USER CODE BEGIN StartReadout2Feature */
  uint32_t last_valid_event = NO_EVENT;
  uint32_t valid_event = NO_EVENT;

  readout.waitTime = 0;

  /* Infinite loop */
  for(;;)
  {
#if DEBUG_MODE_ON
    if (readout.waitTime > MAX_WAIT_TIME)
    {
      AW9523_LED_CTRL(LED_ALL_OFF);
    }
#endif
    if(readout.flag == 1)
    {
      readout.flag = 0;
      readout.event = getEvent();

      if (readout.event == LEFT_EVENT || readout.event == RIGHT_EVENT)
      {
#if DEBUG_MODE_ON
        /* light up LEDs to display event */
        if (readout.event == LEFT_EVENT)
        {
          AW9523_LED_CTRL(LED_LEFT_ON);
        }
        else
        {
          AW9523_LED_CTRL(LED_RIGHT_ON);
        }
#endif
        valid_event = readout.event;
        if (last_valid_event == NO_EVENT)
        {
          readout.waitTime = 0;
          last_valid_event = valid_event;
        }
        else if (last_valid_event == valid_event)
        {
          readout.waitTime = 0;
        }
        else if (last_valid_event != valid_event)
        {
          if (readout.waitTime < MAX_WAIT_TIME)
          {
            if ((last_valid_event == RIGHT_EVENT) && (valid_event == LEFT_EVENT))
            {
              readout.result = IN_DOOR;
              readout.inDoorCnt++;
            }
            else if ((last_valid_event == LEFT_EVENT) && (valid_event == RIGHT_EVENT))
            {
              readout.result = OUT_DOOR;
              readout.outDoorCnt++;
            }
            last_valid_event = NO_EVENT;
#if DEBUG_MODE_ON
            AW9523_LED_CTRL(LED_ALL_OFF);
            AW9523_LED_CTRL_CENTER(0x05);
#endif
            BUZZER_ON();
            osDelay(100);
            BUZZER_OFF();
            osDelay(SILENCE_TIME - 100);
#if DEBUG_MODE_ON
            AW9523_LED_CTRL_CENTER(0);
#endif
          }
          else
          {
            last_valid_event = valid_event;
          }
        }
      }
    }
    else
    {
      osDelay(10);
    }
  }
  /* USER CODE END StartReadout2Feature */
}
