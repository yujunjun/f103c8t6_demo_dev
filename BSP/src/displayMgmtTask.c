#include "cmsis_os.h"

#include "bsp_readout.h"
#include "bsp_aw9523.h"
#include "bsp_buzzer.h"
#include "DEV_Config.h"
#include "EPD_3in7.h"
#include "ePaper.h"
#include "main.h"

static void SystemClock_ReConfig(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

void StartDisplayMgmt(void const * argument)
{
  /* USER CODE BEGIN 5 */
#if USE_USART_TO_DISPALY
  extern UART_HandleTypeDef huart2;
  UART_HandleTypeDef *uart = &huart2;
  uint8_t str_in[]  = "t0.txt=\"+0000\"";
  uint8_t str_out[] = "t1.txt=\"-0000\"";
  uint8_t hexEnd[3] = {0xFF, 0xFF, 0xFF};
#endif
  uint16_t inDoorCnt = 0;
  uint16_t outDoorCnt = 0;

  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
  SystemClock_ReConfig();

  /* Infinite loop */
  for(;;)
  {
#if USE_USART_TO_DISPALY
    if (inDoorCnt != readout.inDoorCnt)
    {
      inDoorCnt = readout.inDoorCnt;

      str_in[9]  = inDoorCnt / 1000 + '0';
      str_in[10] = (inDoorCnt / 100) % 10 + '0';
      str_in[11] = (inDoorCnt / 10) % 10 + '0';
      str_in[12] = inDoorCnt % 10 + '0';
      HAL_UART_Transmit(uart, str_in, 14, 1000);
      HAL_UART_Transmit(uart, hexEnd, 3, 1000);
    }
    else if (outDoorCnt != readout.outDoorCnt)
    {
      outDoorCnt = readout.outDoorCnt;

      str_out[9]  = outDoorCnt / 1000 + '0';
      str_out[10] = (outDoorCnt / 100) % 10 + '0';
      str_out[11] = (outDoorCnt / 10) % 10 + '0';
      str_out[12] = outDoorCnt % 10 + '0';
      HAL_UART_Transmit(uart, str_out, 14, 1000);
      HAL_UART_Transmit(uart, hexEnd, 3, 1000);
    }
    else
    {
      if (readout.waitTime > MAX_WAIT_TIME)
      {
#if DEBUG_MODE_ON
        AW9523_LED_CTRL(LED_ALL_OFF);
#endif
        BUZZER_OFF();
        HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
        SystemClock_ReConfig();
        osDelay(1000);
      }
    }
#else
    if (readout.refresh == 1)
    {
      EPD_3IN7_4Gray_Init();
      EPD_3IN7_4Gray_Clear();
      DEV_Delay_ms(1000);
      EPD_3IN7_1Gray_Init();
      ePaper_update_number(readout.inDoorCnt, readout.outDoorCnt);
      EPD_3IN7_1Gray_Clear();
      readout.refresh = 0;
    }
    else
    {
      if (inDoorCnt != readout.inDoorCnt || outDoorCnt != readout.outDoorCnt)
      {
        inDoorCnt = readout.inDoorCnt;
        outDoorCnt = readout.outDoorCnt;
        ePaper_update_number(inDoorCnt, outDoorCnt);
        EPD_3IN7_1Gray_Display(gImage_body);
        osDelay(600);
      }
      else
      {
        if (readout.waitTime > MAX_WAIT_TIME)
        {
#if DEBUG_MODE_ON
          AW9523_LED_CTRL(LED_ALL_OFF);
#endif
          BUZZER_OFF();
          HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
          SystemClock_ReConfig();
          osDelay(1000);
        }
      }
    }
#endif
  }
  /* USER CODE END 5 */
}
