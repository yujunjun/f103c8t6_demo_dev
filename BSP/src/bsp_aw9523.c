#include "bsp_aw9523.h"

extern I2C_HandleTypeDef hi2c1;

static void AW9523_Reset(void);
static void AW9523_Check(void);
static void AW9523_Set_LED_Mode(void);

void AW9523_Init(void)
{
  AW9523_Reset();
  AW9523_Check();
  AW9523_Set_LED_Mode();
  AW9523_LED_CTRL(LED_ALL_ON);
  HAL_Delay(500);
  AW9523_LED_CTRL(LED_ALL_OFF);
}

static void AW9523_Reset(void)
{
  HAL_GPIO_WritePin(AW9523_RST_GPIO_Port, AW9523_RST_Pin,GPIO_PIN_RESET);
  HAL_Delay(1);
  HAL_GPIO_WritePin(AW9523_RST_GPIO_Port, AW9523_RST_Pin,GPIO_PIN_SET);
}

static void AW9523_Check(void)
{
  uint8_t value = 0;

  HAL_I2C_Mem_Read(&hi2c1, AW9523_RADDR, AW9523_ID, 1, &value, 1, 1000);
}

static void AW9523_Set_LED_Mode(void)
{
  uint8_t value = 0;

  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED_MODE0, 1, &value, 1, 1000);
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED_MODE1, 1, &value, 1, 1000);
  value = 0x03;
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_CTRL, 1, &value, 1, 1000);
}

void AW9523_LED_CTRL(uint16_t pos)
{
  uint8_t i = 0;
  uint8_t idim = 0;

  for (i = 0; i < 16; i++)
  {
    if (i == 7 || i == 11 || i == 5 || i == 9 || i == 6 || i == 10)
    {
      continue;
    }
    if (pos & (1 << i))
    {
      idim = AW9523_LED_BRIGHTNESS;
    }
    else
    {
      idim = 0;
    }
    HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + i, 1, &idim, 1, 1);
  }
}

void AW9523_LED_CTRL_7_11(uint8_t idim)
{
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 7, 1, &idim, 1, 1);
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 11, 1, &idim, 1, 1);
}

void AW9523_LED_CTRL_CENTER(uint8_t idim)
{
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 5, 1, &idim, 1, 1);
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 9, 1, &idim, 1, 1);
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 6, 1, &idim, 1, 1);
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + 10, 1, &idim, 1, 1);
}

void AW9523_LED_CTRL_SINGLE(uint8_t led_n, uint8_t on_off)
{
  HAL_I2C_Mem_Write(&hi2c1, AW9523_WADDR, AW9523_LED0 + led_n, 1, &on_off, 1, 1);
}
