#include "GUI_Paint.h"
#include "DEV_Config.h"
PAINT Paint;
void Paint_NewImage(UBYTE *image, UWORD Width, UWORD Height, UWORD Rotate, UWORD Color)
{
  Paint.Image = NULL;
  Paint.Image = image;

  Paint.WidthMemory = Width;
  Paint.HeightMemory = Height;
  Paint.Color = Color;

  Paint.Scale = 2;
  Paint.WidthByte = (Width % 8 == 0)? (Width / 8 ): (Width / 8 + 1);
  Paint.HeightByte = Height;
 
  Paint.Rotate = Rotate;
  Paint.Mirror = 0;

  Paint.Width = Width;
  Paint.Height = Height;
}
void Paint_Clear(UWORD Color)
{
	if(Paint.Scale == 2 || Paint.Scale == 4){
		for (UWORD Y = 0; Y < Paint.HeightByte; Y++) {
			for (UWORD X = 0; X < Paint.WidthByte; X++ ) {//8 pixel =  1 byte
				UDOUBLE Addr = X + Y*Paint.WidthByte;
				Paint.Image[Addr] = Color;
			}
		}		
	}else if(Paint.Scale == 7){
		for (UWORD Y = 0; Y < Paint.HeightByte; Y++) {
			for (UWORD X = 0; X < Paint.WidthByte; X++ ) {
				UDOUBLE Addr = X + Y*Paint.WidthByte;
				Paint.Image[Addr] = (Color<<4)|Color;
			}
		}		
	}
}
void Paint_SelectImage(UBYTE *image)
{
    Paint.Image = image;
}
void Paint_DrawBitMap(const unsigned char* image_buffer)
{
    UWORD x, y;
    UDOUBLE Addr = 0;

    for (y = 0; y < Paint.HeightByte; y++) {
        for (x = 0; x < Paint.WidthByte; x++) {//8 pixel =  1 byte
            Addr = x + y * Paint.WidthByte;
            Paint.Image[Addr] = (unsigned char)image_buffer[Addr];
        }
    }
}
